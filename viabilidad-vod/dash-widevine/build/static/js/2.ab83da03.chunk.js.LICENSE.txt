/*
    @license
    Copyright 2006 The Closure Library Authors
    SPDX-License-Identifier: Apache-2.0
    */

/*
    @license
    Copyright 2008 The Closure Library Authors
    SPDX-License-Identifier: Apache-2.0
    */

/*
    @license
    EME Encryption Scheme Polyfill
    Copyright 2019 Google LLC
    SPDX-License-Identifier: Apache-2.0
    */

/*
    @license
    Shaka Player
    Copyright 2016 Google LLC
    SPDX-License-Identifier: Apache-2.0
    */

/*
 @license
 Shaka Player
 Copyright 2016 Google LLC
 SPDX-License-Identifier: Apache-2.0
*/

/*
object-assign
(c) Sindre Sorhus
@license MIT
*/

/** @license React v0.19.1
 * scheduler.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/** @license React v16.13.1
 * react-dom.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/** @license React v16.13.1
 * react.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
