import React, { Component } from "react";
import Init from "./components/init";
import ShakaPlayer from "./components/shaka";

class App extends Component {
  state = {
    showInit: true,
    showShakaPlayer: false,
    lastStartTime: 0,
  };

  handleKeyPressInit = (key, timestamp) => {
    const showInit = false;
    const showShakaPlayer = true;
    const lastStartTime = timestamp;
    //console.log(lastStartTime);
    this.setState({ showInit, showShakaPlayer, lastStartTime });
  };

  handleKeyPressShakaPlayer = (key) => {
    const showInit = true;
    const showShakaPlayer = false;
    this.setState({ showInit, showShakaPlayer });
  };

  handleVideoLoad = (timestamp) => {
    console.log(
      "It took " +
        (timestamp - this.state["lastStartTime"]).toString(10) +
        " miliseconds to start the video."
    );
  };

  render() {
    //console.log("App - Rendered");

    document.body.style = "background: black;";

    return (
      <React.Fragment>
        {this.state.showInit && <Init onKeyPress={this.handleKeyPressInit} />}
        {this.state.showShakaPlayer && (
          <ShakaPlayer
            onKeyPress={this.handleKeyPressShakaPlayer}
            onVideoLoad={this.handleVideoLoad}
          />
        )}
      </React.Fragment>
    );
  }
}

export default App;
