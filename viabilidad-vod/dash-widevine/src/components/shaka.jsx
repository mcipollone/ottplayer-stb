/*
  Links útiles:
    - https://github.com/google/shaka-player/issues/2163
    - https://github.com/amit08255/shaka-player-react-with-ui-config
  */
//importing dependencies
import React from "react";
import shaka from "shaka-player";
import "./shaka.css";

//Creating class component

class ShakaPlayer extends React.PureComponent {
  constructor(props) {
    super(props);

    //Creating reference which will allow access to video player on DOM
    this.videoComponent = React.createRef();

    //Adding reference to event handler functions
    this.onErrorEvent = this.onErrorEvent.bind(this);
    this.onError = this.onError.bind(this);
    this.KeyPress = this.KeyPress.bind(this);
  }

  onErrorEvent(event) {
    // Extract the shaka.util.Error object from the event.
    this.onError(event.detail);
  }

  onError(error) {
    // Log the error.
    console.error("Error code", error.code, "object", error);
  }

  //Initialize your shaka player here
  componentDidMount() {
    //MPEG-DASH video URL
    var manifestUri =
      //"https://storage.googleapis.com/shaka-demo-assets/angel-one/dash.mpd";
      //"https://cdn.stg.cvattv.com.ar/live/live2/FXHD/SA_Live_dash_fta/FXHD.mpd";
      "https://cdn.stg.cvattv.com.ar/vod/vod/ott_vod/FOX_Tomb_Raider_Las_Aventuras_De_Lara_C_HD_STR_SUB/_/vod_dash_enc/FOX_Tomb_Raider_Las_Aventuras_De_Lara_C_HD_STR_SUB.mpd";

    //Reference to our video component on DOM
    const video = this.videoComponent.current;

    //Initializing our shaka player
    var player = new shaka.Player(video);

    // Attach player to the window to make it easy to access in the JS console.
    window.player = player;

    // Configuración del License adquisition server de Widevine para pruebas de Guille.
    player.configure({
      drm: {
        servers: {
          "com.widevine.alpha":
            "https://wv-client.stg.cvattv.com.ar?deviceId=MzAyNDc4MTdDRUQ3",
          "com.microsoft.playready": "https://foo.bar/drm/playready",
        },
        advanced: {
          "com.widevine.alpha": {
            videoRobustness: "",
            audioRobustness: "",
          },
        },
      },
    });

    // Listen for error events.
    player.addEventListener("error", this.onErrorEvent);

    // Try to load a manifest.
    // This is an asynchronous process.
    player
      .load(manifestUri)
      .then(() => {
        console.log("The video has now been loaded!");
        this.props.onVideoLoad(new Date().getTime());
      })
      .catch(this.onError); // onError is executed if the asynchronous load fails.
  }

  componentWillMount() {
    //console.log("ShakaPlayer - Mounted");
    document.addEventListener("keydown", this.KeyPress);
  }

  componentWillUnmount() {
    //console.log("ShakaPlayer - UnMounted");
    document.removeEventListener("keydown", this.KeyPress);
  }

  KeyPress(event) {
    //console.log("ShakaPlayer - Key pressed " + event.keyCode);
    this.props.onKeyPress(event);
  }

  render() {
    /*Returning video component. Shaka player will be added to this component
            once its mounted on DOM
        */
    return <video className="video" ref={this.videoComponent} autoPlay />;
  }
}

export default ShakaPlayer;
