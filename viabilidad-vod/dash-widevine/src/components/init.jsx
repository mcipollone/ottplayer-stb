import React, { Component } from "react";
import "./init.css";

class Init extends Component {
  state = {};

  constructor(props) {
    super(props);
    this.KeyPress = this.KeyPress.bind(this);
  }

  componentWillMount() {
    //console.log("Init - Mounted");
    document.addEventListener("keydown", this.KeyPress);
  }

  componentWillUnmount() {
    //console.log("Init - UnMounted");
    document.removeEventListener("keydown", this.KeyPress);
  }

  KeyPress(event) {
    //console.log("Init - Key pressed " + event.keyCode);
    //console.log("Playback requested");
    this.props.onKeyPress(event, new Date().getTime());
  }

  render() {
    return <div className="ButtonFocus">Start Test</div>;
  }
}

export default Init;
