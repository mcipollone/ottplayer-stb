import React, { Component } from "react";
import Init from "./components/init";
import Native from "./components/native";

class App extends Component {
  state = {
    showInit: true,
    showNative: false,
    lastStartTime: 0,
  };

  handleKeyPressInit = (key, timestamp) => {
    const showInit = false;
    const showNative = true;
    const lastStartTime = timestamp;
    //console.log(lastStartTime);
    this.setState({ showInit, showNative, lastStartTime });
  };

  handleKeyPressNative = (key) => {
    const showInit = true;
    const showNative = false;
    this.setState({ showInit, showNative });
  };

  handleVideoLoad = (timestamp) => {
    console.log(
      "It took " +
        (timestamp - this.state["lastStartTime"]).toString(10) +
        " miliseconds to start the video."
    );
  };

  render() {
    //console.log("App - Rendered");

    document.body.style = "background: black;";

    return (
      <React.Fragment>
        {this.state.showInit && <Init onKeyPress={this.handleKeyPressInit} />}
        {this.state.showNative && (
          <Native
            onKeyPress={this.handleKeyPressNative}
            onVideoLoad={this.handleVideoLoad}
          />
        )}
      </React.Fragment>
    );
  }
}

export default App;
