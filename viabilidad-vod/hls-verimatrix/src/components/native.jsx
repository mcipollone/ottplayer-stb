import React, { Component } from "react";
import "./native.css";

class Native extends Component {
  state = {};

  constructor(props) {
    super(props);
    this.videoComponent = React.createRef();
    this.KeyPress = this.KeyPress.bind(this);
  }

  componentDidMount() {
    const video = this.videoComponent.current;

    console.log(video);

    //video.pause();
    video.src =
      "http://cdn.cvattv.com.ar/live/c2live/Food_Network/verimatrix_rotating/Food_Network.m3u8";

    video.onloadeddata = () => {
      this.props.onVideoLoad(new Date().getTime());
    };

    video.load();
  }

  componentWillMount() {
    //console.log("Native - Mounted");
    this.props.onVideoLoad(new Date().getTime());
    document.addEventListener("keydown", this.KeyPress);
  }

  componentWillUnmount() {
    //console.log("Native - UnMounted");
    document.removeEventListener("keydown", this.KeyPress);
  }

  KeyPress(event) {
    //console.log("Native - Key pressed " + event.keyCode);
    this.props.onKeyPress();
  }

  render() {
    //return <video className="video" src={video} controls autoPlay />;

    return (
      <video
        className="video"
        ref={this.videoComponent}
        //src="http://cdn.cvattv.com.ar/live/c2live/Food_Network/verimatrix_rotating/Food_Network.m3u8"
        src=""
        autoPlay
      />
    );
  }
}

export default Native;
