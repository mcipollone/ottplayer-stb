#! /bin/bash
printf "timestamp,memory,cpu,load\n"

timestamp() {
  date +"%T"
}

while true; do
MEMORY=$(free -m | awk 'NR==2{printf "%.2f", $3*100/$2 }')
CPU=$(top -bn1| grep CPU | grep -v grep| grep -v STAT | awk '{printf "%.2f",(100-$(NF-7))}')
LOAD=$(top -bn1 | grep Load | grep -v grep | awk '{printf "%.2f", $(NF-4)}')
TIME=$(timestamp)
echo "$TIME,$MEMORY,$CPU,$LOAD"
sleep 2
done
